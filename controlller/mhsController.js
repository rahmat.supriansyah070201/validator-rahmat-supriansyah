const { validationResult } = require('express-validator');

exports.getData=(req,res,next)=>{
    res.status(200).json({
        id:"1",
        name:"Rahmat Supriansyah",
        classes :"3SI"
    })
}

exports.createPosts = (req, res, next)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    const name = req.body.name;
    const classes = req.body.classes;
    const campus = req.body.campus;
    const gender = req.body.gender;
    res.status(201).json({
        message: 'Post created Successfully',
        biodata: { id: new Date().toISOString(), 
            name: name, 
            classes: classes,
            campus: campus,
            gender: gender
        }
    });
}
