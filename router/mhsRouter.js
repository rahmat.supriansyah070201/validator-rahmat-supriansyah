const exp = require("express");
const { body } = require("express-validator")
const router = exp.Router();
const mhsController =require("../controlller/mhsController");

router.get("/getObjMhs",mhsController.getData);
router.post("/post", [
    body('name').isLength({ min: 5 }),
    body('classes').isLength({ min: 2 }),
    body('campus').isLength({ min: 5 }),
    body('gender').isLength({ min: 5 })

], mhsController.createPosts);

module.exports=router;