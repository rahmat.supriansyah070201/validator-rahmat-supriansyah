const exp = require("express")
const bodyParser = require('body-parser');
const app= exp();
const port=8080;

const mhsRouter=require("./router/mhsRouter");

app.use("/mhs",mhsRouter);
app.use(bodyParser.json());
app.listen(port,function(){
    
    console.log("App Is Running on Port "+port);
})